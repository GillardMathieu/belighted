module Api
    module V1
        class TeamPlayersController < ApplicationController 
            def index
                team_players = TeamPlayer.order('id')
                render json: {status: 'SUCCESS', message: 'Loaded team_players', data:team_players },status: :ok
            end

            def show
                team_player = TeamPlayer.find(params[:id])
                render json: {status: 'SUCCESS', message: 'Loaded team_player', data:team_player },status: :ok
            end

            def create
                team_player = TeamPlayer.new(team_player_params)
                
                if team_player.save
                    render json: {status: 'SUCCESS', message: 'Saved team_player', data:team_player },status: :ok
                else
                    render error: {error: 'Impossible to create a team_player'}, status: 400
                end
            end

            def destroy
                team_player = TeamPlayer.find(params[:id])
                team_player.destroy
                render json: {status: 'SUCCESS', message: 'Deleted team_player', data:team_player },status: :ok
            end

            def update
                team_player = TeamPlayer.find(params[:id])
                if team_player.update_attributes(team_player_params)
                    render json: {status: 'SUCCESS', message: 'Updated team_player', data:team_player },status: :ok
                else
                    render error: {error: 'Impossible to update this team_player'}, status: 400
                end 
            end 

            private

            def team_player_params
                params.require(:team_player).permit(:team_id, :player_id)
            end
        end
    end
end