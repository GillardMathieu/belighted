module Api
    module V1
        class TeamsController < ApplicationController 
            def index
                teams = Team.order('id');
                render json: {status: 'SUCCESS', message: 'Loaded teams', data:teams },status: :ok
            end

            def show
                team = Team.find(params[:id])
                render json: {status: 'SUCCESS', message: 'Loaded team', data:team },status: :ok
            end

            def create
                team = Team.new(team_params)
                
                if team.save
                    render json: {status: 'SUCCESS', message: 'Saved team', data:team },status: :ok
                else
                    render error: {error: 'Impossible to create a team'}, status: 400
                end
            end

            def destroy
                team = Team.find(params[:id])
                team.destroy
                render json: {status: 'SUCCESS', message: 'Deleted team', data:team },status: :ok
            end

            def update
                team = Team.find(params[:id])
                if team.update_attributes(team_params)
                    render json: {status: 'SUCCESS', message: 'Updated team', data:team },status: :ok
                else
                    render error: {error: 'Impossible to update this team'}, status: 400
                end 
            end 

            private

            def team_params
                params.require(:team).permit(:Name, :TrainingDay1, :TrainingDay2)
            end
        end
    end
end