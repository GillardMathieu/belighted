module Api
    module V1
        class TrainingPlayersController < ApplicationController 
            def index
                team_players = TrainingPlayer.order('id')
                render json: {status: 'SUCCESS', message: 'Loaded training_players', data:team_players },status: :ok
            end

            def last10

                teamid = (params[:teamid]).to_i

                players = Team.find(teamid).players

                training_ids = TeamTraining.where(team_id: teamid).where("DATE(trainingdate) <= DATE(?)", Time.now).order('trainingdate DESC').limit(10).ids
                
                
                results = Array.new
                
                for p in players
                    nbPresence = Player.find(p.id).training_players.where(team_training_id: training_ids).where(IsPresent: true).count()
                    nbAbsence = Player.find(p.id).training_players.where(team_training_id: training_ids).where(IsPresent: false).count()
                    
                    nb_match = nbPresence + nbAbsence

                    results.push(PlayerStat.new(p.id, p.Name, nbPresence*100 / nb_match))
                end
                
                
                render json: {status: 'SUCCESS', message: 'Ratio for last 10 trainings', data:results },status: :ok
            end

            def season

                teamid = (params[:teamid]).to_i

                players = Team.find(teamid).players

                training_ids = TeamTraining.where(team_id: teamid)
                .where("DATE(trainingdate) <= DATE(?)", Time.now)
                .where("DATE(trainingdate) >= DATE(?)", Date.new(Time.now.year, 1,1))
                .order('trainingdate DESC').ids
                
                
                results = Array.new
                
                for p in players
                    nbPresence = Player.find(p.id).training_players.where(team_training_id: training_ids).where(IsPresent: true).count()
                    nbAbsence = Player.find(p.id).training_players.where(team_training_id: training_ids).where(IsPresent: false).count()
                    
                    nb_match = nbPresence + nbAbsence

                    results.push(PlayerStat.new(p.id, p.Name, nbPresence*100 / nb_match))
                end
                
                
                render json: {status: 'SUCCESS', message: 'Ratio for the season', data:results },status: :ok
            end

            # def show
            #     team_player = TrainingPlayer.find(params[:id])
            #     render json: {status: 'SUCCESS', message: 'Loaded team_player', data:team_player },status: :ok
            # end

            # def create
            #     team_player = TrainingPlayer.new(team_player_params)
                
            #     if team_player.save
            #         render json: {status: 'SUCCESS', message: 'Saved team_player', data:team_player },status: :ok
            #     else
            #         render error: {error: 'Impossible to create a team_player'}, status: 400
            #     end
            # end

            # def destroy
            #     team_player = TrainingPlayer.find(params[:id])
            #     team_player.destroy
            #     render json: {status: 'SUCCESS', message: 'Deleted team_player', data:team_player },status: :ok
            # end

            def update
                team_player = TrainingPlayer.find(params[:id])
                if team_player.update_attributes(team_player_params)
                    render json: {status: 'SUCCESS', message: 'Updated training_player', data:team_player },status: :ok
                else
                    render error: {error: 'Impossible to update this training_player'}, status: 400
                end 
            end 

            private

            def team_player_params
                params.require(:training_player).permit(:team_training_id, :player_id, :IsPresent)
            end
        end
    end
end