module Api
    module V1
        class TeamTrainingsController < ApplicationController 
            def index
                teamid = (params[:teamid]).to_i
                
                if teamid != 0
                    team_trainings = TeamTraining.where(team_id: teamid).where("DATE(trainingdate) >= DATE(?)", Date.today).order('id');
                else
                    team_trainings = TeamTraining.order('id');
                end
                render json: {status: 'SUCCESS', message: 'Loaded team_trainings', data:team_trainings },status: :ok
            end

            def show
                team_training = TeamTraining.find(params[:id])
                render json: {status: 'SUCCESS', message: 'Loaded team_training', data:team_training },status: :ok
            end

            def create
                team_training = TeamTraining.new(team_training_params)
                
                if team_training.save

                    players = Team.find(team_training.team_id).players

                    for p in players
                        training_player = TrainingPlayer.new()
                        training_player.team_training_id = team_training.id
                        training_player.player_id = p.id
                        training_player.IsPresent = p.PresentByDefault

                        training_player.save
                    end

                    render json: {status: 'SUCCESS', message: 'Saved team_training', data:team_training },status: :ok
                else
                    render error: {error: 'Impossible to create a team_training'}, status: 400
                end
            end

            def createWithLimit

                $number_of_recurrency = (params[:limit]).to_i

                team_id = team_training_params[:team_id]

                sended_date = team_training_params[:trainingdate]

                date = Date.parse(sended_date)

                trainingday1 = Team.where(id: team_id).first.TrainingDay1
                trainingday2 = Team.where(id: team_id).first.TrainingDay2

                diff = trainingday1 - date.wday

                date += (diff + 7) % 7

                for i in 1..$number_of_recurrency do                    
                    
                    team_training = TeamTraining.new()
                    team_training.team_id= team_id
                    team_training.trainingdate = date

                    team_training.save

                    players = Team.find(team_id).players

                    for j in players
                        training_player = TrainingPlayer.new()
                        training_player.team_training_id = team_training.id
                        training_player.player_id = j.id
                        training_player.IsPresent = j.PresentByDefault

                        training_player.save
                    end

                    date += 7
                end
                if !trainingday2.nil?
                    date2 = Date.parse(sended_date)

                    diff2 = trainingday2 - date2.wday

                    date2 += (diff2 + 7) % 7
                    for i in 1..$number_of_recurrency do                    
                    
                        team_training = TeamTraining.new()
                        team_training.team_id= team_id
                        team_training.trainingdate = date2
    
                        team_training.save
    
                        players = Team.find(team_id).players
    
                        for j in players
                            training_player = TrainingPlayer.new()
                            training_player.team_training_id = team_training.id
                            training_player.player_id = j.id
                            training_player.IsPresent = j.PresentByDefault
    
                            training_player.save
                        end
    
                        date2 += 7
                    end
                end
            end

            def destroy
                team_training = TeamTraining.find(params[:id])
                team_training.destroy
                render json: {status: 'SUCCESS', message: 'Deleted team_training', data:team_training },status: :ok
            end

            def update
                team_training = TeamTraining.find(params[:id])
                if team_training.update_attributes(team_training_params)
                    render json: {status: 'SUCCESS', message: 'Updated team_training', data:team_training },status: :ok
                else
                    render error: {error: 'Impossible to update this team_training'}, status: 400
                end 
            end 

            private

            def team_training_params
                params.require(:team_training).permit(:team_id, :trainingdate)
            end
        end
    end
end