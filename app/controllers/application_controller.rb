class ApplicationController < ActionController::Base
    skip_before_action :verify_authenticity_token
    def error(status, code, message)
        render :js => {:response_type => "ERROR", :response_code => code, :message => message}.to_json, :status => status
      end
end
