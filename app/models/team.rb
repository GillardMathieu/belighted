class Team < ApplicationRecord
    has_many :team_players, dependent: :destroy
    has_many :players , :through => :team_players

    # validate :TwoSessionPerDate
    validate :UniqueTeamName

    # def TwoSessionPerDate

    #     totald1 = 0

    #     nb_at_day1 = Team.where(TrainingDay1: self.TrainingDay1).length
    #     nb_at_day2 = Team.where(TrainingDay2: self.TrainingDay1).length
    #     totald1 = nb_at_day1 + nb_at_day2

    #     if !self.TrainingDay2.nil?
    #         totald2 = 0

    #         nb2_at_day1 = Team.where(TrainingDay1: self.TrainingDay2).length
    #         nb2_at_day2 = Team.where(TrainingDay2: self.TrainingDay2).length
    #         totald2 = nb2_at_day1 + nb2_at_day2
    #     end
        
    #     if totald1 >= 2
    #         raise '2 sessions per date is permit'
    #     end

    #     if totald2 >= 2
    #         raise '2 sessions per date is permit'
    #     end
    # end

    def UniqueTeamName
        if Team.where(Name: self.Name).length >= 1
            raise 'Another team is already registered with that name'
        end
    end


end
