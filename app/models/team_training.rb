class TeamTraining < ApplicationRecord
    validates :team_id, presence: true

    has_many :teams
    has_many :players , :through => :training_players
    has_many :training_players, dependent: :destroy

    validate :UniqueTeamPerDate
    validate :MaxTwoTeamsPerDate

    def UniqueTeamPerDate
        if TeamTraining.where(team_id: self.team_id).where("DATE(trainingdate) = DATE(?)", self.trainingdate).length >= 1
            raise "A team can't make a training once per date"
        end
    end
    def MaxTwoTeamsPerDate
        if TeamTraining.where("DATE(trainingdate) = DATE(?)", self.trainingdate).length >= 2
            raise "2 teams maximum per date"
        end
    end
    
end