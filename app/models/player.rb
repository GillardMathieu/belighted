class Player < ApplicationRecord
    has_many :team_players, dependent: :destroy
    has_many :teams , :through => :team_players

    has_many :training_players, dependent: :destroy
    has_many :team_trainings , :through => :training_players

    validate :UniqueFullName

    def UniqueFullName
        if Player.where(Name: self.Name).length >= 1
            raise 'Another player is already registered with that name'
        end
    end
end
