class PlayerStat
  def initialize(id, name, ratio)
      @id = id
      @name = name
      @ratio = ratio
  end

  def ratio
      @ratio
  end

  attr_reader :name
end