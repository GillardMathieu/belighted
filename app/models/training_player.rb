class TrainingPlayer < ApplicationRecord
    belongs_to :team_training
    belongs_to :player

    validate :CheckDateForUpdate

    def CheckDateForUpdate
        if TeamTraining.where(id: self.team_training_id).where("DATE(trainingdate) < DATE(?)", Time.now).length >= 1
            raise 'Unable to update a training session for a player before today'
        end
    end
end