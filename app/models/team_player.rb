class TeamPlayer < ApplicationRecord
    belongs_to :team
    belongs_to :player

    validates :team_id, presence: true
    validates :player_id, presence: true

    validate :MaxTwoTeams
    validate :UniqueTeamEntry

    def MaxTwoTeams
        if TeamPlayer.where(player_id: player_id).length >= 2
            raise 'The player have more than 2 teams'
        end
    end

    def UniqueTeamEntry
        if TeamPlayer.where(player_id: player_id).where(team_id: team_id).length >=1
            raise 'A player can only play once for a team'
        end
    end

    
end