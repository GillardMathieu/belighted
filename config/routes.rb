Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do

      post 'team_trainings/recurrent/' => 'team_trainings#createWithLimit'
      get 'training_players/last10' => 'training_players#last10'
      get 'training_players/season' => 'training_players#season'

      resources :teams
      resources :players
      resources :team_players
      resources :team_trainings
      resources :training_players
    end 
  end
end
