class CreateTeamPlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :team_players do |t|
      t.references :team, foreign_key: true, null: false
      t.references :player, foreign_key: true, null: false

      t.timestamps
    end
  end
end
