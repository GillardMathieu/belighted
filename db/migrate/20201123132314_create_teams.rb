class CreateTeams < ActiveRecord::Migration[6.0]
  def change
    create_table :teams do |t|
      t.string :Name
      t.integer :TrainingDay1, null: false
      t.integer :TrainingDay2

      t.timestamps
    end
  end
end
