class CreateTeamTrainings < ActiveRecord::Migration[6.0]
  def change
    create_table :team_trainings do |t|
      t.references :team, foreign_key: true, null: false
      t.date :trainingdate
      
      t.timestamps
    end
  end
end
