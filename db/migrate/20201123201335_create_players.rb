class CreatePlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :players do |t|
      t.string :Name
      t.boolean :PresentByDefault

      t.timestamps
    end
  end
end
