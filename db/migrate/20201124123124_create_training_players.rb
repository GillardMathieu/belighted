class CreateTrainingPlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :training_players do |t|
      t.references :team_training, foreign_key: true, null: false
      t.references :player, foreign_key: true, null: false
      t.boolean :IsPresent

      t.timestamps
    end
  end
end
