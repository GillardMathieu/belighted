# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_24_123124) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "players", force: :cascade do |t|
    t.string "Name"
    t.boolean "PresentByDefault"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "team_players", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.bigint "player_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["player_id"], name: "index_team_players_on_player_id"
    t.index ["team_id"], name: "index_team_players_on_team_id"
  end

  create_table "team_trainings", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.date "trainingdate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_team_trainings_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "Name"
    t.integer "TrainingDay1", null: false
    t.integer "TrainingDay2"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "training_players", force: :cascade do |t|
    t.bigint "team_training_id", null: false
    t.bigint "player_id", null: false
    t.boolean "IsPresent"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["player_id"], name: "index_training_players_on_player_id"
    t.index ["team_training_id"], name: "index_training_players_on_team_training_id"
  end

  add_foreign_key "team_players", "players"
  add_foreign_key "team_players", "teams"
  add_foreign_key "team_trainings", "teams"
  add_foreign_key "training_players", "players"
  add_foreign_key "training_players", "team_trainings"
end
